package org.vanis.playground;

public class Main {

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            try {
                if (i % 2 == 1) throw new BusinessRule.LineItemMustExist(i + " violates this business rule");
            } catch (BusinessRule.Violation br){
                br.printStackTrace();
            }
            try {
                if (i % 3 == 1) throw new TechnicalRule.MessageDoesNotConformToSchemaDefinition(i + " violates this rule");
            } catch (TechnicalRule.Violation tr){
                tr.printStackTrace();
            }
        }
    }
}
