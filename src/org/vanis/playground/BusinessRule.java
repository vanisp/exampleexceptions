package org.vanis.playground;

interface BusinessRule {

    class Violation extends RuntimeException {
        public Violation() {
        }

        Violation(String message) {
            super(message);
        }
    }

    class LineItemMustExist extends Violation {
        public LineItemMustExist() {
        }

        public LineItemMustExist(String message) {
            super(message);
        }

    }

    class LineItemMustNotBeCanceled extends Violation {

        public LineItemMustNotBeCanceled() {
        }

        public LineItemMustNotBeCanceled(String message) {
            super(message);
        }
    }


}
