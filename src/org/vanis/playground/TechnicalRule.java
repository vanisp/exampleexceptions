package org.vanis.playground;

interface TechnicalRule {

    class Violation extends RuntimeException {

        public Violation() {
        }

        public Violation(String message) {
            super(message);
        }
    }

    class MessageDoesNotConformToSchemaDefinition extends Violation {

        public MessageDoesNotConformToSchemaDefinition() {
        }

        public MessageDoesNotConformToSchemaDefinition(String message) {
            super(message);
        }
    }

    class NullMessage extends Violation {

        public NullMessage() {
        }

        public NullMessage(String message) {
            super(message);
        }
    }
}
